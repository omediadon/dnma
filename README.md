# DNMA par UAI

## Termes
**DNMA**: Agrégation multiple basée sur la double normalisation; un outil des suivi/analyse 

**UAI**: Le code unique *Unité Administrative Immatriculée*, composé de 7 chiffres et d’une lettre

## Installation
### Prequists
 1. 14 < Nodejs < 18
 2. Yarn *(optional)*

### Steps
 1. Clone this repo `git clone https://gitlab.com/omediadon/dnma.git`
 2. Install the dependencies `yarn install` is preferred over `npm install`
 3. Start the app `yaen start`

## CI et Docker
### Docker
Dockerfile is present in the app, you can build the environment (image) through `docker build -t "NAME:Dockerfile" .`

### CI
CI is provided through the use of GitlabCI configured on `.gitlab-ci.yml`
