import {Component} from "react";
import {Link, Switch} from "react-router-dom";
import {Route} from "react-router-dom";
import Home from "../components/home";
import Evolution from "../components/evolution";
import TopMenu from "../components/common/TopMenu";

class RoutesX extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <>
                <TopMenu />
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/evolution" component={Evolution}/>
                    <Route exact path="/" component={Home}/>
                </Switch>
            </>
        );
    }
}

export default RoutesX;
