const API_URL = "https://data.education.gouv.fr/api/explore/v2.1/";

export const apiConfig = {
    dataset: "fr-en-dnma-par-uai-appareils",
    routes: {
        getData: API_URL + "catalog/datasets/$DATASET$/records"
    }
};
