import * as Actions from "./actionTypes";
import dataServices from "../../services/data.services";

/**
 * This action get the data by device
 * @param uai
 * @param device
 * @return {(function(*): void)|*}
 */
export const getDataForDevice = (uai, device) => {
    return (dispatch) => {
        dataServices.getData(uai, device).then((data) =>
            dispatch({
                type: Actions.GET_DATA_BY_DEVICE,
                post: data
            })
        );
    };
};
