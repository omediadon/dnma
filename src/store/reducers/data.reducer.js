import * as Actions from "../actions/actionTypes";

const initialState = {
    data: null
};

const DataReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_DATA_BY_DEVICE: {
            return {
                ...state,
                data: action.data
            };
        }

        default:
            return state;
    }
};

export default DataReducer;
