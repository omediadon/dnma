import { combineReducers } from "redux";
import DataReducer from "./data.reducer";

const rootReducer = combineReducers({
  apiData: DataReducer
});

export default rootReducer;
