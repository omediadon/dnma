import {Component} from "react";
import {getDataForDevice} from "../../store/actions/data.actions";
import {connect} from "react-redux";

class Evolution extends Component {
    constructor(props) {
        super(props);
        this.state = {
            device: "visites_ordinateur",
            uai: ""
        };
        this.updateUAI = this.updateUAI.bind(this);
        this.updateDevice = this.updateDevice.bind(this);
    }


    updateUAI(evt) {
        this.state.uai = evt.target.value;
    }

    updateDevice(evt) {
        this.state.device = evt.target.value;
    }


    getContent = () => {
        this.props.getDataForDevice(this.state.uai, this.state.device);
    };

    render() {
        let {data} = this.props;

        return (
            <div>
                <label htmlFor={"uai"}>UAI</label><input id={"uai"} onChange={this.updateUAI}/>
                <label htmlFor={"device"}>Appareil</label>
                <select id={"device"} onChange={this.updateDevice}>
                    <option value={"visites_ordinateur"}>Ordinateur</option>
                    <option value={"visites_tablette"}>Tablette</option>
                    <option value={"visites_smartphone"}>Smartphone</option>
                </select>
                <button onClick={() => this.getContent()}>get the data</button>
                {data && <code>{JSON.stringify(data)}</code>}
            </div>
        );
    }
}

Evolution.propTypes = {};

const mapStateToProps = (state) => {
    return {
        data: state.apiData.data
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getDataForDevice: (uai, device) => dispatch(getDataForDevice(uai, device))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Evolution);
