import React from "react";
import PropTypes from "prop-types";
import {Link, NavLink} from "react-router-dom";

function TopMenu(props) {

    return (
        <div className={"menu"}>
            <ul>
                <li><NavLink exact activeClassName={"active"} to={"/"}>Accueil</NavLink></li>
                <li><NavLink activeClassName={"active"} to={"/granu"}>Visites sur UAI par Granularité</NavLink></li>
                <li><NavLink activeClassName={"active"} to={"/evolution"}>Evolution des visites sur UAI par année</NavLink>
                </li>
            </ul>
        </div>
    );
}

TopMenu.propTypes = {
    theme: PropTypes.string
};

export default TopMenu;
