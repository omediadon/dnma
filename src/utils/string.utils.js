class StringUtils {
    /**
     * Reformat a string with placeholders
     *
     * @param source a string with placeholders formatted as $name$
     * @param params an object with property names matching the placeholders
     *
     * @return string formatted string
     */
     replacePlaceholders=(source, params)=> {
         let placeholders = source.match(/\$(.*?)\$/g)

         placeholders.forEach(function(placeholder){
             let phText = placeholder.substring(1,placeholder.length - 1);

             if(params[phText]){
                 source = source.replace(placeholder,params[phText])
             }
         })

         return source
     }
}

const instance = new StringUtils();

export default instance;