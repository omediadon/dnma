import axios from "axios";
import {apiConfig} from "../config/api.config";
import stringUtils from "../utils/string.utils"

class DataServices {
    /**
     * gets the data from the backend based on the provided query generated from the args
     *
     * If a selection is provided, this function will only query for said selection plus the date
     * The data can always be limited by year
     *
     *
     * @returns {Promise<unknown>}
     */
    getData = (uai, selection, annee) => {
        const acceptedSelects = ["visites_smartphone", "visites_tablette", "visites_ordinateur"]
        if (uai == null || (selection != null && !acceptedSelects.includes(selection))) {
            return new Promise((resolve, reject) => reject("missing required params"))
        }

        const url = stringUtils.replacePlaceholders(apiConfig.routes.getData, {"DATASET": apiConfig.dataset})
        let where = `uai='${uai}'`
        let select = ""

        if (annee != null) where += `and date'${annee}'`
        if (selection != null) select = selection + ",debutsemaine"

        let params = {
            select: select,
            where: where,
            offset: 0,
            limit: -1,
            timezone: "UTC",
            include_links: false,
            include_app_metas: false
        }

        return new Promise((resolve, reject) => {
            axios
                .get(url, {
                    params: params
                })
                .then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    console.error(error);
                    reject(error);
                });
        });
    };
}

const instance = new DataServices();

export default instance;
